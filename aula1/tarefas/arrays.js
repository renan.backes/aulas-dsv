// Tarefa 1
// Arrays
lista = []
lista.push( "Drenan" )
lista.push( "crenan teste syonet" )
lista.push( "Brenan backes" )
lista.push("Arenan back" )

numberList = [];
numberList.push(1, 2, 3, 4, 5, 6, 7 );
console.log("=============================");
console.log("ARRAYS");
console.log("=============================");
console.log("\n");
//1 - Array.filter Crie uma lista de textos e filtre os que tem mais de 10 caracteres Array.filter
function filterListChar( lista ) {
    return lista.filter( function( elem  ) {
        return elem.length > 10;
    });
}
console.log( filterListChar( lista ));
console.log("=============================");
console.log("=============================");

//2 - Array.map - Crie uma lista de textos e reverta o conteudo deles use 
function mapListReverter( lista ) {
    return lista.map( function( elem ) {
        return elem.split('').reverse().join('');
    });
}
novaLista = mapListReverter( lista ) 
console.log( novaLista );
console.log("=============================");
console.log("=============================");

//3 - Array.sort - Crie uma lista de textos e ordene de ordem alfabética
// Por ser case Sensitive, deve ser feito um lower ou upper case
//Ou poder ser usado o metodo localeCompare()
function sortListAscendent ( lista ) {
    return lista.sort( function ( elemA, elemB ) {
        //Com Case Sensitive
         // return elemA < elemB ? -1 : elemA > elemB ? 1 : 0;
        // Sem case Sensitive
         return elemA.localeCompare(elemB);
    });
}
console.log( sortListAscendent( lista ) );
console.log("=============================");
console.log("=============================");

//4 -  Array.reduce - Crie uma lista de numeros e some o valor de todos eles
function reduceListPlus( numberList ) {
    return numberList.reduce( function (acumlatedValue, elem ) {
        return acumlatedValue + elem;
    });
}
console.log( reduceListPlus( numberList ) );
console.log("=============================");
console.log("=============================");