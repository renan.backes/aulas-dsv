// Resolução do Codigo
function removeItens(lista, exlusoes ) {
    for (var i =0; i<lista.length; i++ ) {
        if ( exlusoes.indexOf(lista[i] ) > -1 ) {
            lista.splice(i,1)
            i--;
        }
    }
    console.log(lista)
}
removeItens([ 1,2,3,4,5 ],[1,2])
removeItens([ 1,2,3,4,5 ],[1,3])
removeItens([ 1,2,3,4,5 ],[3,4]) 
console.log("=============================");
console.log("=============================");

//Explicando o bug
// Ao cair na condição do If, um elemento será retirado.
// Assim, o indice deverá ser diminuido também
// Para que nao pule o proximo a ser pego.
// Em resumo, da forma como estava, se o segundo fosse removido
// O terceiro indice, da lista original não seria analisado.
// Corrigido com a linha 6 "i--";
function removeItensFilter(lista, exlusoes ) {
        for (var i =0; i<lista.length; i++ ) {
            if ( exlusoes.indexOf(lista[i] ) > -1 ) {
              lista =  lista.filter(function( elem ) {
                    return elem != lista[i]
                })
                i--;
            }
        }        
    
    console.log(lista)
}
removeItensFilter([ 1,2,3,4,5 ],[1,2])
removeItensFilter([ 1,2,3,4,5 ],[1,3])
removeItensFilter([ 1,2,3,4,5 ],[3,4])