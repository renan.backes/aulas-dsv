//Tarefa 1
// Funções

//1 - Crie uma funcao para multiplicar dados
function multiplica( valor, valor2 ) {
    return valor * valor2;
}
console.log("O valor da multiplicacao e: " + multiplica( 10, 20) );

//2 - Crie uma funcao para subtrair dados
function subtrai( menos1, menos2) {
    return menos1 - menos2;
}
console.log("O valor da subtracao e: " + subtrai( 20, 10) );

//3 - Crie uma funcao para reverter uma string
// Explicando a função
// o metodo split, esta dividindo em um array cada palavra onde possui um espaço,
// Como o reverse recebe um array, ele inverte os valores
// Após inversao, ocorre a junção com o join novamente, colocando espaços no meio
function reverteString( texto ) {
   return texto.split('').reverse().join('');
}
console.log('\"' + reverteString( "Renan1 Backes2 Syonet3 ") + "\"");

//4 - Crie uma funcao para remover as vogais de uma string
var novatexto;
function removeVogais( texto ) {
    return  texto.replace( /[aeiouà-ú]/gi, '')
}
novatexto = removeVogais("Renan Backes");
console.log(novatexto);

