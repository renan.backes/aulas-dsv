function somar(number1, number2) {
    return number1 + number2;
}
var fun = somar;
console.log("==================");
console.log("EXERCICIO 1");
console.log("" + fun(10, 20));
console.log("==================");
// Crie um método que receba uma lista de palavras e retorne um novo array com a quantidade de caracteres.
var lista = ["Drnadn", "crenan teste syonet", "Brenan backes", "Arenanback", "teste"];
function contaCaracteres(valores) {
    return valores.map(function (elem) {
        return elem.length;
    });
}
var novaListaDeCaraceteres = contaCaracteres;
console.log("EXERCICIO 2");
console.log(" " + novaListaDeCaraceteres(lista) + " ");
console.log("==================");
// Crie um método que retorna a quantidade de anos, meses, dias, horas, minutos, segundos, de intervalo entre duas datas.
var dataInicial = new Date(2018, 5, 1, 0, 0, 0).getTime();
var dataFinal = new Date(2018, 5, 25, 0, 0, 0).getTime();
function calculaIntervaloDias(initialDate, finalDate) {
    //Aqui imprime o totalizador de cada tipo de datetime
    var aux = (finalDate - initialDate);
    var newSegundos = aux / 1000;
    var newMinutos = newSegundos / 60;
    var newHoras = newMinutos / 60;
    var newDias = newHoras / 24;
    var newMeses = newDias / 30;
    var newAnos = newMeses / 12;
    return ("Tem " + newAnos.toFixed(2) + " anos " +
        "ou " + newMeses.toFixed(2) + " meses " +
        "ou " + newDias.toFixed() + " dias " +
        "ou " + newHoras.toFixed() + " horas " +
        "ou " + newMinutos.toFixed() + " minutos " +
        "ou " + newSegundos.toFixed() + " segundos");
}
var novoCalculoIntervaloDias = calculaIntervaloDias;
console.log("EXERCICIO 3");
console.log("" + novoCalculoIntervaloDias(dataInicial, dataFinal));
console.log("==================");
function multiplica(param1, param2) {
    return param1 * param2;
}
function subtrai(param1, param2) {
    return param1 - param2;
}
function dividi(param1, param2) {
    return param1 / param2;
}
function principal(callback) {
    var valores = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        valores[_i - 1] = arguments[_i];
    }
    return valores.reduce(callback);
}
// Nesse caso, no primeiro console.log, para demonstração
// Optei em colocar o type tanto para o metodo de fora quanto a função de callback
// Com o type do primeiro exercicio, se quiser, da para fazer para os outros;
var calcular = principal;
var somaCalculadora = somar;
console.log("EXERCICIO 4");
console.log("SOMA >> " + calcular(somaCalculadora, 10));
console.log("DIVIDI >> " + ("" + calcular(dividi, 9, 3)));
console.log("MULTIPLICA >> " + ("" + calcular(multiplica, 3, 3)));
console.log("SUBTRAI >> " + ("" + calcular(subtrai, 10, 3)));
console.log("==================");
