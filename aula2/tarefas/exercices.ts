
// Crie um método que some dois argumentos respeitando a seguinte tipagem:
type somaType = ( parameter1:number, parameter2:number ) => number;
function somar( number1:number, number2:number ): number {
        return number1 + number2;
}
let fun: somaType = somar;
console.log("==================")
console.log("EXERCICIO 1")
console.log(`${ fun(10, 20) }`);
console.log("==================")

// Crie um método que receba uma lista de palavras e retorne um novo array com a quantidade de caracteres.
let lista: Array<string> = ["Drnadn","crenan teste syonet", "Brenan backes", "Arenanback","teste" ];

type listaDeCaracteres = ( parameter: Array<string> ) => Array<number>;

function contaCaracteres( valores: Array<string> ): Array<number> {
    return valores.map( function( elem:string ): number {
        return elem.length;
    })
}
let novaListaDeCaraceteres: listaDeCaracteres = contaCaracteres;
console.log("EXERCICIO 2")
console.log(` ${novaListaDeCaraceteres( lista )} `);
console.log("==================")
// Crie um método que retorna a quantidade de anos, meses, dias, horas, minutos, segundos, de intervalo entre duas datas.
let dataInicial:number = new Date(2018, 5, 1, 0, 0, 0).getTime();
let dataFinal:number = new Date(2018, 5, 25, 0, 0, 0).getTime();
type intervaloDiasType = ( parameter1:number, parameter2:number ) => string

function calculaIntervaloDias( initialDate:number, finalDate:number ) {
        //Aqui imprime o totalizador de cada tipo de datetime
        let aux:number = (finalDate - initialDate);
        let newSegundos:number = aux /1000;
        let newMinutos:number = newSegundos /60;
        let newHoras:number = newMinutos /60;
        let newDias:number = newHoras /24;
        let newMeses:number = newDias /30;
        let newAnos:number = newMeses /12;
        return ("Tem " + newAnos.toFixed(2) + " anos " +
                    "ou " + newMeses.toFixed(2) + " meses " +
                    "ou " + newDias.toFixed() + " dias " +
                    "ou " + newHoras.toFixed() + " horas " +
                    "ou " + newMinutos.toFixed() + " minutos " +
                    "ou " + newSegundos.toFixed() + " segundos" );
}
let novoCalculoIntervaloDias: intervaloDiasType = calculaIntervaloDias;
console.log("EXERCICIO 3")
console.log( `${ novoCalculoIntervaloDias( dataInicial, dataFinal ) }` );
console.log("==================")

// Crie um método chamado calculadora que receberá uma lista de argumentos + função callback que fara o cálculo desejado.
type calculadoraType = ( callback:somaType, ...parameters:number[] ) => number

function multiplica( param1:number, param2:number ):number {
    return param1 * param2;
}
function subtrai( param1:number, param2:number ):number {
    return param1 - param2;
}
function dividi( param1:number, param2:number ):number {
    return param1 / param2;
}

function principal( callback: somaType, ...valores: number[] ) {
    return valores.reduce( callback );
}
// Nesse caso, no primeiro console.log, para demonstração
// Optei em colocar o type tanto para o metodo de fora quanto a função de callback
// Com o type do primeiro exercicio, se quiser, da para fazer para os outros;
let calcular: calculadoraType = principal;
let somaCalculadora: somaType = somar;
console.log( "EXERCICIO 4" )
console.log( `SOMA >> ${ calcular(somaCalculadora, 10, 3, 1) }` );
console.log( "DIVIDI >> " + `${ calcular(dividi, 9, 3) }`) ;
console.log( "MULTIPLICA >> " + `${ calcular(multiplica, 3, 3) }` );
console.log( "SUBTRAI >> " + `${ calcular(subtrai, 10, 3) }` );
console.log("==================")