//Tarefa 1
// Funções;
//1 - Crie uma funcao para multiplicar dados
function multiplica(param1, param2) {
    return param1 * param2;
}
console.log("O valor da multiplicacao e: " + multiplica(10, 20));
function substrai(param1, param2) {
    return param1 - param2;
}
console.log("O valor da substracao e: " + substrai(20, 10) + " ");
function reverteString(param1) {
    return param1.split('').reverse().join('');
}
console.log("\"" + reverteString("Renan1 Backes2 Syonet3 ") + " \" ");
var lista = [];
lista.push("Drenan");
lista.push("crenan teste syonet");
lista.push("Brenan backes");
lista.push("Arenan back");
console.log("=============================");
console.log("ARRAYS");
console.log("=============================");
console.log("\n");
//1 - Array.filter Crie uma lista de textos e filtre os que tem mais de 10 caracteres Array.filter
function filterListChar(lista) {
    return lista.filter(function (elem) {
        return elem.length > 10;
    });
}
console.log(" " + filterListChar(lista) + " ");
console.log("=============================");
console.log("=============================");
//2 - Array.map - Crie uma lista de textos e reverta o conteudo deles use 
function mapListReverter(lista) {
    return lista.map(function (elem) {
        return elem.split('').reverse().join('');
    });
}
console.log(" " + mapListReverter(lista) + " ");
//////////// Tipo
var fun;
var exp;
function soma(param1, param2) {
    return param1 + param2;
}
function subtrai(param1, param2) {
    return param1 - param2;
}
function divir(param1, param2) {
    return param1 / param2;
}
function multiplicar(param1, param2) {
    return param1 * param2;
}
function exponencial(param1, param2) {
    return Math.pow(param1, param2);
}
function exponencial2(param1, param2) {
    var saida = param1;
    if (param1 === 0 || param2 === 0) {
        return 1;
    }
    else {
        for (var i = 1; i < param2; i++) {
            saida *= param1;
        }
        return saida;
    }
}
fun = soma;
console.log(" " + fun(10, 20) + " ");
fun = subtrai;
console.log(" " + fun(20, 10) + " ");
fun = divir;
console.log(" " + fun(20, 10) + " ");
fun = multiplicar;
console.log(" " + fun(20, 10) + " ");
exp = exponencial;
console.log(" " + exp(2, 0) + " ");
exp = exponencial2;
console.log(" " + exp(2, 0) + " ");
