var Futebol = /** @class */ (function () {
    function Futebol(tipo, cheia) {
        this.cheia = cheia;
        this.tipo = tipo;
    }
    Futebol.prototype.usar = function (confeccao) {
        this.cheia === true ? console.log("Chute a bola pro gol") : console.log("Encha a bola de Futebol");
    };
    return Futebol;
}());
var Basquete = /** @class */ (function () {
    function Basquete(tipo, cheia) {
        this.cheia = cheia;
        this.tipo = tipo;
    }
    Basquete.prototype.usar = function (confeccao) {
        this.cheia === true ? console.log("Faça a sexta") : console.log("Encha a bola de Basquete");
    };
    return Basquete;
}());
var ConfeccaoBola;
(function (ConfeccaoBola) {
    ConfeccaoBola["COURO"] = "COURO";
    ConfeccaoBola["BORRACHA"] = "BORRACHA";
})(ConfeccaoBola || (ConfeccaoBola = {}));
var futebol = new Futebol("Bola de Futebol", false);
var tiposDeBolas = [new Futebol("Bola de Futebol", true),
    new Basquete("Bola de Basquete", true)];
futebol.usar(ConfeccaoBola.BORRACHA);
tiposDeBolas[0].usar(ConfeccaoBola.BORRACHA);
tiposDeBolas[1].usar(ConfeccaoBola.COURO);
