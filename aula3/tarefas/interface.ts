interface Bola {
    tipo: string;
    cheia: boolean;
    usar : (confeccao: ConfeccaoBola) => void;
}

class Futebol implements Bola {
    tipo: string;
    cheia: boolean;
    constructor(tipo:string, cheia:boolean) {
        this.cheia = cheia;
        this.tipo = tipo;
    }
    usar(confeccao: ConfeccaoBola) {
        this.cheia === true ?  console.log("Chute a bola pro gol") : console.log("Encha a bola de Futebol") 
    }    
}

class Basquete implements Bola {
    tipo: string;
    cheia: boolean;
    constructor(tipo:string, cheia:boolean) {
        this.cheia = cheia;
        this.tipo = tipo;
    }
    usar(confeccao: ConfeccaoBola) {
        this.cheia === true ?  console.log("Faça a sexta") : console.log("Encha a bola de Basquete") 
    } 
}
enum ConfeccaoBola {
    COURO = "COURO",
    BORRACHA = "BORRACHA",
}

var futebol:Bola = new Futebol( "Bola de Futebol", false );
let tiposDeBolas:Array<Bola> = [ new Futebol( "Bola de Futebol", true ),
                            new Basquete( "Bola de Basquete", true ) ]
futebol.usar(ConfeccaoBola.BORRACHA);
tiposDeBolas[0].usar(ConfeccaoBola.BORRACHA);
tiposDeBolas[1].usar(ConfeccaoBola.COURO);

