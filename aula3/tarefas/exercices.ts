// Criação das interfaces
interface Organico { decompoe(): string }
interface Vegetal  { alimentar: ( decompositores: Decompositores ) => void }
interface Inseto {  alimentar: ( alimento: Vegetal,  digestao: ( alimento:Vegetal ) => Organico ) => void }
interface Decompositores { alimentar: ( type: Vegetal ) => void; }

function digestao( alimento: Vegetal ) {
    let organico:Organico = new Merda();
    return organico;
}
function decompoe( param: string ) {

}

//Criação das classes
class Merda implements Organico {
    decompoe(): string { return '' }
}
let texto:string = "Começou a cagar";
class Planta implements Vegetal, Organico {
     alimentar( decompositores: Decompositores ) {
        console.log(`É uma planta que se alimenta de decompositores na terra`);
     }
     decompoe() {
         return texto;
     }

}
type Digestao = ( alimento:Vegetal ) => Organico; 
class Gafanhoto implements Inseto, Organico {
    alimentar( vegetal: Vegetal, digestao: Digestao ) {
        console.log(`É um gafanhoto do tipo Inseto que se alimenta de vegetais`);
    }
    decompoe() {
        return texto;
    }

}
let planta:Vegetal = new Planta();
let gafanhoto:Inseto = new Gafanhoto();
gafanhoto.alimentar( planta, digestao )